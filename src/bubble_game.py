# bubble-game.py
#
# Copyright 2023 Jan Willem
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later



from gi.repository import Gtk, Gio, Adw, GLib, GObject, Graphene, Gdk, Gsk
from typing import Any, Optional
from enum import IntEnum
import random
from typing import List, Set, Dict, Tuple
import time
import math


COLORS = []
color = Gdk.RGBA()
color.parse('rgba(0,0,0,0)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(0,0,255)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(255,0,0)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(0,255,0)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(255,255,0)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(0,255,255)')
COLORS.append(color)
color = color.copy()
color.parse('rgb(255,0,255)')
COLORS.append(color)

class Tile(IntEnum):
    EMPTY = 0
    BLUE = 1
    RED = 2
    GREEN = 3
    YELLOW = 4
    LIGHT_BLUE = 5
    PURPLE = 6

class GameState(IntEnum):
    INIT = 0
    READY = 1
    RUNNING = 2
    SHOOT = 3
    REMOVE = 4
    PAUSED = 5
    FINISHED = 6

@Gtk.Template(resource_path='/io/gitlab/Bubbles/gtk/lives.ui')
class Lives(Gtk.Widget):
    __gtype_name__ = 'Lives'

    BUBBLE_RADIUS = GObject.Property(type=int, minimum=6, maximum=48, default=12)

    max_lives = GObject.Property(type=int, minimum=0, maximum=6, default=4)
    lives_value = 4;
    lives_bubbles = []

    @GObject.Property(type=int, minimum=0, default=4)
    def lives(self) -> int:
        return self.lives_value

    @lives.setter
    def lives(self, value: int) -> None:
        if self.lives != value and value <= self.max_lives:

            for i in range(self.max_lives):
                if i >= value:
                    self.lives_bubbles[i].set_visible(False)
                else:
                    self.lives_bubbles[i].set_visible(True)

            self.lives_value = value


    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        for live in range(self.max_lives):
            bubble = Bubble()
            bubble.set_parent(self)
            self.lives_bubbles.append(bubble)




class Bubble(Gtk.Widget):
    __gtype_name__ = 'Bubble'

    color = GObject.Property(type=int, minimum=-1, maximum=6, default=1)

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.set_size_request(30, 30)


    def do_snapshot(self, s: Gtk.Snapshot):
        if self.color == Tile.EMPTY:
            return

        width = self.get_width()
        height = self.get_height()

        rect = Graphene.Rect().init(0, 0, width, height)
        bubble_clip = Gsk.RoundedRect()
        bubble_clip.init_from_rect(rect, radius= round(width / 2))
        s.push_rounded_clip(bubble_clip)
        s.append_color(COLORS[self.color], rect)
        s.pop()



class BubbleBoard(Gtk.Widget):
    __gtype_name__ = 'BubbleBoard'

    columns = GObject.Property(type=int, minimum=4, maximum=30, default=17)
    rows = GObject.Property(type=int, minimum=4, maximum=30, default=17)

    BUBBLE_RADIUS = GObject.Property(type=int, minimum=6, maximum=48, default=15)
    BUBBLE_SPACING = 3


    @Gtk.Template.Callback()
    def get_bubble_diameter(self, *args) -> int:
        return self.BUBBLE_RADIUS * 2


    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.set_layout_manager(BoardLayoutManager())

        self.tiles = [[Bubble(color=Tile.EMPTY) for y in range(self.rows)] for x in range(self.columns)]

        for x in range(self.columns):
            for y in range(self.rows):
                bubble = self.tiles[x][y]
                bubble.set_parent(self)
                if y <= 10:
                    bubble.color=random.randint(2, len(Tile)-1)

        self.connect('notify::BUBBLE-RADIUS', self.bubble_radius_update)

    def bubble_radius_update(self, *args):
        self.BUBBLE_SPACING = self.BUBBLE_RADIUS * 0.25
        self.queue_resize()



    # def reset_tiles(self, seed) -> None:

        # All are initialized to 0: EMPTY
    #     self.tiles = [[None for y in range(self.rows)] for x in range(self.columns)]

    #     for y in range(4):
    #         for x in range(self.columns):
    #             self.tiles[x][y] = random.randint(1, len(Tile)-1)

    def get_valid_random_ballon(self) -> Tile:
        return random.randint(1, len(Tile)-1)

    def get_tile_coordinate(self, column, row) -> tuple[float, float]:

        tilex = column * (self.BUBBLE_RADIUS * 2 + self.BUBBLE_SPACING)

        # X offset for odd rows
        if row % 2:
            tilex += self.BUBBLE_RADIUS

        tiley = row * (self.BUBBLE_RADIUS * 2)
        return tilex, tiley

    def get_tile(self, column, row) -> Optional[Bubble]:
        if column < 0 or column >= self.columns:
            return None

        if row < 0 or row >= self.rows:
            return None

        return self.tiles[column][row]



    # Returns the tiles that a Bubble position (top, right) covers
    # If the bubble perfectly aligns with grid, extra tiles are returned
    def get_tiles_for_pos(self, pos):
        (pos_x, pos_y) = pos
        tiles = []

        (column, row) = self.get_tile_for_pos(pos)
        for x in range(-1, 3):
            for y in range(-1, 2):
                tile = self.get_tile(column + x, row + y)
                if tile != None:
                    tiles.append(tile)
        return tiles


    def get_tile_for_pos(self, pos: tuple[float, float]):
        (x, y) = pos
        row = math.floor(y / (self.BUBBLE_RADIUS * 2))
        column = math.floor((x - (row % 2) * self.BUBBLE_RADIUS) / (self.BUBBLE_RADIUS * 2 + self.BUBBLE_SPACING))
        return column, row

    # Gets all bubbles that a bubble at a given position collides with
    # @var pos: top left corner of bubble
    def collides_with(self, pos: tuple[float, float]):
        tiles = self.get_tiles_for_pos(pos)
        colidees = []
        for tile in tiles:
            if tile.color == Tile.EMPTY:
                continue

            if self.tile_collides(tile, pos):
                colidees.append(tile)
        return colidees

    # If a bubble at column, row collides with a bubble at pos
    # This is done using the distance function
    # @var pos: top left corner of bubble
    def tile_collides(self, tile, pos: tuple[float, float]):
        (res, bounds) = tile.compute_bounds(self)
        tile_x = bounds.get_x()
        tile_y = bounds.get_y()
        return math.sqrt((tile_x - pos[0])**2 + (tile_y - pos[1])**2) <= self.BUBBLE_RADIUS * 2


class BoardLayoutManager(Gtk.LayoutManager):

    last_bubble_radius = None
    last_bubble_spacing = None

    def do_allocate(self, widget, width, height, baseline):
        if widget.BUBBLE_RADIUS == self.last_bubble_radius and widget.BUBBLE_SPACING == self.last_bubble_spacing:
            # if the radius or spacing did not change then so will the bubble allocation not change, thus return
            print('no radius or spacing change', self.last_bubble_radius, self.last_bubble_spacing)
            return

        self.last_bubble_radius = widget.BUBBLE_RADIUS
        self.last_bubble_spacing = widget.BUBBLE_SPACING


        for x in range(widget.columns):
            for y in range(widget.rows):
                bubble = widget.tiles[x][y]

                if bubble is None:
                    continue

                coords = widget.get_tile_coordinate(x, y)

                allocation = Gdk.Rectangle()
                allocation.x = coords[0]
                allocation.y = coords[1]
                allocation.width = widget.BUBBLE_RADIUS * 2
                allocation.height= widget.BUBBLE_RADIUS * 2
                bubble.size_allocate(allocation, -1)

    def do_measure(self, widget, orientation, for_size):
        if orientation == Gtk.Orientation.HORIZONTAL:
            width = widget.BUBBLE_RADIUS * 2 * widget.columns + widget.BUBBLE_RADIUS + ((widget.columns -1 ) * widget.BUBBLE_SPACING)
            return width, width, -1, -1
        else:
            height = widget.BUBBLE_RADIUS * 2 * widget.rows
            return height, height, -1, -1



@Gtk.Template(resource_path='/io/gitlab/Bubbles/gtk/bubble-game.ui')
class BubbleGame(Gtk.Widget):
    __gtype_name__ = 'BubbleGame'


    bubble_board = Gtk.Template.Child()
    lives = Gtk.Template.Child()
    arrow = Gtk.Template.Child()
    container = Gtk.Template.Child()
    shooting_bubble = Gtk.Template.Child()

    # Shooting pos is in percentage of the total width and height,
    # This allows us tho resize the widget without having to recalculate the position
    shooting_pos = (0.5, 1)
    shooting_angle = None
    shooting_speed = 1.5

    rotation = 0

    game_state_value = GameState.INIT
    seed_value = None
    difficulty = GObject.Property(type=int, minimum=1, maximum=20, default=1)
    score = GObject.Property(type=int, minimum=0, default=0)
    turns = GObject.Property(type=int, minimum=0, default=0)
    # lives = GObject.Property(type=int, minimum=0, maximum=5, default=0)

    bubble_queue = []

    pointer_position: Optional[tuple[float, float]] = None

    shooting_bubble_layout = None
    previous_time = None


    @GObject.Property(type=int, minimum=GameState.INIT, maximum=len(GameState)-1, default=GameState.INIT, flags=GObject.ParamFlags.READABLE)
    def game_state(self):
        return self.game_state_value

    @GObject.Property(type=str, flags=GObject.ParamFlags.READABLE)
    def seed(self):
        return self.seed_value


    def _set_game_state(self, game_state: GameState) -> None:
        self.game_state_value = game_state
        self.notify('game_state')

    def _set_seed(self, seed: str) -> None:
        self.seed_value = seed
        self.notify('seed')

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)


        self.connect('notify::rows', self.reset)
        self.connect('notify::columns', self.reset)
        self.reset()

        motion_controller = Gtk.EventControllerMotion()
        motion_controller.connect('motion', self.on_motion)
        self.add_controller(motion_controller)

        click_controller = Gtk.GestureClick()
        click_controller.connect('released', self.on_click)
        self.add_controller(click_controller)

        self.connect('map', self.on_mapped)

    def on_mapped(self, t):
        print('map', self.get_width())

    def on_enter(self, event: Gtk.EventControllerMotion, x: float, y: float) -> None:
        self.pointer_position = (x, y)

    def on_motion(self, event: Gtk.EventControllerMotion, x: float, y: float) -> None:
        self.pointer_position = (x, y)
        (arrow_x, arrow_y) = self.get_arrow_pos()

        mouse_rad = math.atan2(arrow_y - y, x - arrow_x)
        mouse_deg = mouse_rad * (180 / math.pi)

        if (mouse_deg < 0):
            mouse_deg = 180 + (180 + mouse_deg)

        self.rotation = mouse_deg
        self.queue_draw()
        return

    def on_leave(self, event: Gtk.EventControllerMotion) -> None:
        return

    def on_click(self, event: Gtk.GestureClick, n_press: int, x: float, y: float) -> None:
        randm = random.randint(0, 4)

        if self.shooting_angle is None:
            if self.rotation < 10:
                self.shooting_angle = 10
            elif self.rotation > 270:
                self.shooting_angle = 10
            elif self.rotation > 170:
                self.shooting_angle = 170
            else:
                self.shooting_angle = self.rotation

            self.shoot()
        else:
            print('shooting')
        self.lives.lives = randm
        self.queue_allocate()
        self.queue_draw()


    def shoot(self):
        calback_id = self.add_tick_callback(self.handle_shoot)

    start = None

    def handle_shoot(self, widget, frame_clock: Gdk.FrameClock):
        if self.previous_time is None:
            self.previous_time = frame_clock.get_frame_time()
            self.start = frame_clock.get_frame_time()
            return True

        dt = frame_clock.get_frame_time() - self.previous_time


        dx = (dt / 1000000) * self.shooting_speed * math.cos(math.radians(self.shooting_angle));
        dy = (dt / 1000000) * self.shooting_speed * -1 * math.sin(math.radians(self.shooting_angle));
        x = self.shooting_pos[0] + dx
        y = self.shooting_pos[1] + dy

        if (x <= 0):
            # Left edge
            self.shooting_angle = 180 - self.shooting_angle
            x = 0
        elif (x >= 1):
            # Right edge
            self.shooting_angle = 180 - self.shooting_angle;
            x = 1;

        if (y <= 0):
            # Top collision
            self.shooting_angle = None
            self.shooting_pos = (0.5, 1)
            self.previous_time = None
            self.queue_allocate()
            self.start = None
            return False;


        self.shooting_pos = (x, y)
        shoot_x = x * (self.get_width() - self.bubble_board.BUBBLE_RADIUS * 2)
        shoot_y = y * (self.get_height() - self.bubble_board.BUBBLE_RADIUS * 2)
        collision_tiles = self.bubble_board.collides_with((shoot_x, shoot_y))
        if len(collision_tiles) > 0:
            if not self.snap_bubble((shoot_x, shoot_y)):
                # The center of the shooting bubble is in a tile that contains a bubble!
                # Use a smaller displacement so we don't end up in a occupied tile!
                x -= dx / 2
                y -= dy / 2
                shoot_x = x * (self.get_width() - self.bubble_board.BUBBLE_RADIUS * 2)
                shoot_y = y * (self.get_height() - self.bubble_board.BUBBLE_RADIUS * 2)
                self.snap_bubble((shoot_x, shoot_y))

            self.queue_allocate()
            self.queue_draw()
            self.shooting_angle = None
            self.shooting_pos = (0.5, 1)
            self.previous_time = None
            return False


        self.queue_allocate()
        self.queue_draw()
        self.previous_time = frame_clock.get_frame_time()
        return True


    # @var pos: top left
    # @return: True if succes
    def snap_bubble(self, pos: tuple[float, float]):
        center_x = pos[0] + self.bubble_board.BUBBLE_RADIUS
        center_y = pos[1] + self.bubble_board.BUBBLE_RADIUS
        (column, row) = self.bubble_board.get_tile_for_pos((center_x, center_y))
        tile = self.bubble_board.get_tile(column, row)
        if tile.color != Tile.EMPTY:
            return False

        tile.color = self.shooting_bubble.color
        tile.queue_draw()
        return True



    def start(self) -> None:
        return

    def pause(self) -> None:
        return

    def reset(self, *args: Any) -> None:
        self._set_game_state(GameState.INIT)
        self.bubble_queue = []

        self._set_seed(random.randint(0, 999999999999))
        random.seed(a=self.seed, version=2)
        # self.bubble_board.reset_tiles(self.seed)

        arrow_x, arrow_y = self.get_arrow_pos()
        # self.fixed.move(self.shooting_bubble, arrow_x, arrow_y)


    def get_arrow_pos(self):
        width = self.get_width()
        height = self.get_height()

        return width / 2, height - self.bubble_board.BUBBLE_RADIUS

    def do_snapshot(self, s):
        self.snapshot_child(self.container, s)
        s.save()
        width = self.get_width()
        height = self.get_height()

        arrow_x, arrow_y = self.get_arrow_pos()
        center = Graphene.Point().init(arrow_x, arrow_y)
        s.translate(center)
        s.rotate(-self.rotation)


        color = Gdk.RGBA()
        color.parse('rgba(0,255,125,1)')
        rect = Graphene.Rect().init(0, -20, 120, 40)
        s.append_color(color, rect)
        s.restore()
        self.snapshot_child(self.shooting_bubble, s)

    def do_size_allocate(self, width, height, baseline):
        allocation = Gdk.Rectangle()
        allocation.x = 0
        allocation.y = 0
        allocation.width = width
        allocation.height = height
        self.container.size_allocate(allocation, -1)

        if self.shooting_pos is None:
            allocation = Gdk.Rectangle()
            allocation.x = (width / 2) - self.bubble_board.BUBBLE_RADIUS
            allocation.y = height - self.bubble_board.BUBBLE_RADIUS * 2
            allocation.width = self.bubble_board.BUBBLE_RADIUS * 2
            allocation.height = self.bubble_board.BUBBLE_RADIUS * 2
            self.shooting_pos = (allocation.x, allocation.y)
            self.shooting_bubble.size_allocate(allocation, -1)
        else:
            allocation = Gdk.Rectangle()
            (shoot_x, shoot_y) = self.shooting_pos
            allocation.x = shoot_x * (width - self.bubble_board.BUBBLE_RADIUS * 2)
            allocation.y = shoot_y * (height - self.bubble_board.BUBBLE_RADIUS * 2)
            allocation.width = self.bubble_board.BUBBLE_RADIUS * 2
            allocation.height = self.bubble_board.BUBBLE_RADIUS * 2
            self.shooting_bubble.size_allocate(allocation, -1)

    def do_measure(self, orientation, for_size):
        return self.container.measure(orientation, for_size)

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.CONSTANT_SIZE
