# main.py
#
# Copyright 2023 Jan Willem
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from typing import Any, cast, Optional, Callable

from gi.repository import Gtk, Gio, Adw, GLib, Gdk
from .window import BubblesWindow
from .bubble_game import Lives
from .bubble_game import BubbleBoard
from .bubble_game import BubbleGame
from .bubble_game import BoardLayoutManager

class BubblesApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self) -> None:
        super().__init__(application_id='io.gitlab.Bubbles',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.create_action('quit', self.quit, ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)
        self.add_css()

    def do_activate(self) -> None:
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win: Gtk.Window = cast(Gtk.Window, self.props.active_window)
        if not win:
            win = BubblesWindow(application=self)
        win.present()

    def quit(self, *args: Any) -> None:
        """ Quit application. """
        win = self.props.active_window
        if win:
            win.destroy()

    def on_about_action(self, *args: Any) -> None:
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=cast(Gtk.Window, self.props.active_window),
                                application_name='bubbles',
                                application_icon='io.gitlab.Bubbles',
                                developer_name='Jan Willem',
                                version='0.1.0',
                                developers=['Jan Willem'],
                                copyright='© 2023 Jan Willem')
        about.present()

    def on_preferences_action(self, *args: Any) -> None:
        """Callback for the app.preferences action."""
        print('app.preferences action activated')

    def create_action(self, name: str, callback: Callable[[Gio.SimpleAction, GLib.Variant], None], shortcuts: Optional[list[str]] = None, param: Optional[GLib.VariantType] = None) -> None:
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


    def add_css(self):
        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource('io/gitlab/Bubbles/gtk/style.css')
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


def main(version: str) -> int:
    """The application's entry point."""
    app = BubblesApplication()
    return app.run(sys.argv)

